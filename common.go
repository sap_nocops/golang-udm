/*
 * Copyright 2014 Canonical Ltd.
 *
 * Authors:
 * Manuel de la Pena: manuel.delapena@canonical.com
 *
 * This file is part of ubuntu-download-manager.
 *
 * ubuntu-download-manager is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-download-manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Package udm provides a go interface to work with the ubuntu download manager
package udm

import (
	"fmt"
	"launchpad.net/go-dbus"
)

// Progress provides how much progress has been performed in a download that was
// already started.
type Progress struct {
	Received uint64
	Total    uint64
}

// internal interface used to simplify testing
type watch interface {
	Cancel() error
	Channel() chan *dbus.Message
}

// small wrapper used to simplify testing by using the watch interface
type watchWrapper struct {
	watch *dbus.SignalWatch
}

func newWatchWrapper(sw *dbus.SignalWatch) *watchWrapper {
	w := watchWrapper{sw}
	return &w
}

func (w *watchWrapper) Cancel() error {
	return w.watch.Cancel()
}

func (w *watchWrapper) Channel() chan *dbus.Message {
	return w.watch.C
}

// interface added to simplify testing
type proxy interface {
	Call(iface, method string, args ...interface{}) (*dbus.Message, error)
}

var readArgs = func(msg *dbus.Message, args ...interface{}) error {
	return msg.Args(args...)
}

func getUint64Value(p proxy, dbusInterface, valueName string) (uint64, error) {
	reply, err := p.Call(dbusInterface, valueName)
	if err != nil {
		return 0, err
	}

	if reply.Type == dbus.TypeError {
		return 0, fmt.Errorf("dbus error: %", reply.ErrorName)
	}

	var value uint64
	if err = readArgs(reply, &value); err != nil {
		return 0, err
	}
	return value, nil
}

func setUint64Value(p proxy, dbusInterface, valueName string, value uint64) error {
	reply, err := p.Call(dbusInterface, valueName, value)
	if err != nil {
		return err
	}

	if reply.Type == dbus.TypeError {
		return fmt.Errorf("dbus error: %", reply.ErrorName)
	}

	return nil
}

func setStringValue(p proxy, dbusInterface, valueName string, value string) error {
	reply, err := p.Call(dbusInterface, valueName, value)
	if err != nil {
		return err
	}

	if reply.Type == dbus.TypeError {
		return fmt.Errorf("dbus error: %", reply.ErrorName)
	}

	return nil
}

func getBoolValue(p proxy, dbusInterface, valueName string) (bool, error) {
	reply, err := p.Call(dbusInterface, valueName)
	if err != nil {
		return false, err
	}

	if reply.Type == dbus.TypeError {
		return false, fmt.Errorf("dbus error: %", reply.ErrorName)
	}

	var value bool
	if err = readArgs(reply, &value); err != nil {
		return false, err
	}
	return value, nil
}

func getMetadataMap(p proxy, dbusInterface, valueName string) (metadata map[string]string, err error) {
	var value map[string]string

	reply, err := p.Call(dbusInterface, valueName)
	if err != nil {
		return value, err
	}

	if reply.Type == dbus.TypeError {
		return value, fmt.Errorf("dbus error: %", reply.ErrorName)
	}

	if err = readArgs(reply, &value); err != nil {
		return value, err
	}

	return value, nil
}

func connectToSignal(conn *dbus.Connection, path dbus.ObjectPath, sender, dbusInterface, signal string) (watch, error) {
	sw, err := conn.WatchSignal(&dbus.MatchRule{
		Type:      dbus.TypeSignal,
		Sender:    sender,
		Interface: dbusInterface,
		Member:    signal,
		Path:      path})
	w := newWatchWrapper(sw)
	return w, err
}
